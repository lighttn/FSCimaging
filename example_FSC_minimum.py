# -*- coding: utf-8 -*-
"""
Created on Tue May 26 12:56:46 2015

@author: jdasilva
"""
import numpy as np
from FSC import *
from scipy import misc #because of lena
import matplotlib.pyplot as plt
#==============================================
# 2D test with random number
print('\n2D test with random number')
img1 = np.random.rand(300,300)
img2 = img1 + np.max(img1)*np.random.rand(300,300)
img1 = img1 + 2*np.max(img1)*np.random.rand(300,300)

FSC2D1 = FSCPlot(img1,img2,0.2071,1)
FSC2D1.plot()
#==============================================
# 2D test with lena
print('\n2D test with lena')
lena = misc.lena()
img1 = lena
img2 = lena + 0.5*np.max(lena)*np.random.rand(lena.shape[0],lena.shape[1])

FSC2D2 = FSCPlot(img1,img2,0.2071,0)
FSC2D2.plot()
#==============================================
# 3D test with random number
print('\n3D test with random number')
img1 = np.random.rand(90,100,80)
img2 = img1 + np.max(img1)*np.random.rand(90,100,80)
img1 = img1 + np.max(img1)*np.random.rand(90,100,80)

FSC3D1 = FSCPlot(img1,img2,0.2071,0)
FSC3D1.plot()

raw_input('<press return to close figures>')
plt.close('all')

#==============================================
# 3D test with a .vol
filename1 = 'cokedcatalyst_25nm_1.vol'
filename2 = 'cokedcatalyst_25nm_2.vol'
# Usually, the file .vol.info contains de size of the volume
linesff = []
infofilename = filename1+'.info'
with open(infofilename,'r') as ff:
    for lines in ff:
        linesff.append(lines.strip('\n'))
x_size = int(linesff[1].split('=')[1])
y_size = int(linesff[2].split('=')[1])
z_size = int(linesff[3].split('=')[1])

# Now we read indeed the .vol file
voldat1 = np.fromfile(filename1, dtype = np.float32).reshape((x_size,y_size,z_size))
voldat2 = np.fromfile(filename2, dtype = np.float32).reshape((x_size,y_size,z_size))

FSC3Dvol = FSCPlot(voldat1,voldat2,0.2071,2)
FSC3Dvol.plot()

raw_input('<press return to close figures>')
plt.close('all')
